// Google Chart
// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(narisiPrazneGrafe);

// ehrScape
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

// Ustvarjene osebe - podatki
/**
   *                           Oseba 1               Oseba 2                               Oseba 3
   * Tip osebe:                Športni navdušenec    Starostnik, hipertenzija, debelost    Mladostnica, nedavna hospitlaizacija
   * Naziv:                    Peter Šilj            Dino Zaver                            Ana Ban
   * Spol:                     Moški                 Moški                                 Ženska
   * Starost let:              35                    60                                    15
   * Teža kg (se spreminja):   78                    106                                   55
   * Višina cm (fiksna):       182                   176                                   170 
   * Sist. tlak mmHg:          116 +- 2              125 - 145                             116 +- 2  +  poslabšanje zadnji teden -> 125 - 145
   * Diast. tlak mmHg:         76 +- 2               85 - 95                               76 +- 2   +  poslabšanje zadnji teden -> 85 - 95
   * Nasičenost O2 %:          97 - 99               90 - 95                               97 - 99   +  poslabšanje zadnji teden -> padanje pod 80
   *
   * Meritve od 1. maj 2019 naprej, dnevno
   */
  var peter_silj = {
    ime: "Peter",
    priimek: "Šilj",
    spol: "MALE",
    rojstvo: "1983-08-16",
    visina: "182",
    meritve: [
      {cas:"2019-05-01", teza:"81.2", diast:"77", sist:"116", kisik:"98.3"},
      {cas:"2019-05-02", teza:"81.0", diast:"75", sist:"117", kisik:"99.2"},
      {cas:"2019-05-03", teza:"80.7", diast:"78", sist:"119", kisik:"97.6"},
      {cas:"2019-05-04", teza:"80.6", diast:"76", sist:"113", kisik:"96.4"},
      {cas:"2019-05-05", teza:"80.4", diast:"77", sist:"115", kisik:"95.3"},
      {cas:"2019-05-06", teza:"80.3", diast:"74", sist:"117", kisik:"98.8"},
      {cas:"2019-05-07", teza:"79.8", diast:"78", sist:"116", kisik:"97.8"},
      {cas:"2019-05-08", teza:"79.5", diast:"79", sist:"116", kisik:"98.7"},
      {cas:"2019-05-09", teza:"79.1", diast:"76", sist:"118", kisik:"96.3"},
      {cas:"2019-05-10", teza:"78.9", diast:"76", sist:"117", kisik:"98.1"},
      {cas:"2019-05-11", teza:"78.7", diast:"78", sist:"114", kisik:"98.3"},
      {cas:"2019-05-12", teza:"78.5", diast:"79", sist:"118", kisik:"97.9"},
      {cas:"2019-05-13", teza:"78.4", diast:"75", sist:"115", kisik:"95.2"},
      {cas:"2019-05-14", teza:"78.5", diast:"74", sist:"116", kisik:"96.7"},
      {cas:"2019-05-15", teza:"78.3", diast:"78", sist:"118", kisik:"97.9"},
      {cas:"2019-05-16", teza:"78.4", diast:"73", sist:"116", kisik:"97.9"},
      {cas:"2019-05-17", teza:"78.1", diast:"76", sist:"115", kisik:"98.3"},
      {cas:"2019-05-18", teza:"77.9", diast:"75", sist:"116", kisik:"99.1"}
    ]
  }

  var dino_zaver = {
    ime: "Dino",
    priimek: "Zaver",
    spol: "MALE",
    rojstvo: "1958-06-23",
    visina: "176",
    meritve: [
      {cas:"2019-05-01", teza:"104.9", diast:"82", sist:"139", kisik:"92.6"},
      {cas:"2019-05-02", teza:"105.1", diast:"88", sist:"141", kisik:"93.4"},
      {cas:"2019-05-03", teza:"105.0", diast:"84", sist:"138", kisik:"92.5"},
      {cas:"2019-05-04", teza:"104.8", diast:"91", sist:"135", kisik:"90.1"},
      {cas:"2019-05-05", teza:"104.9", diast:"90", sist:"126", kisik:"89.8"},
      {cas:"2019-05-06", teza:"105.1", diast:"86", sist:"133", kisik:"92.7"},
      {cas:"2019-05-07", teza:"105.2", diast:"87", sist:"143", kisik:"91.2"},
      {cas:"2019-05-08", teza:"105.5", diast:"92", sist:"144", kisik:"92.4"},
      {cas:"2019-05-09", teza:"105.2", diast:"94", sist:"137", kisik:"90.8"},
      {cas:"2019-05-10", teza:"105.1", diast:"89", sist:"142", kisik:"92.9"},
      {cas:"2019-05-11", teza:"105.4", diast:"93", sist:"128", kisik:"92.8"},
      {cas:"2019-05-12", teza:"106.1", diast:"92", sist:"140", kisik:"91.4"},
      {cas:"2019-05-13", teza:"105.8", diast:"94", sist:"135", kisik:"89.2"},
      {cas:"2019-05-14", teza:"105.6", diast:"89", sist:"126", kisik:"90.5"},
      {cas:"2019-05-15", teza:"104.9", diast:"87", sist:"124", kisik:"91.3"},
      {cas:"2019-05-16", teza:"105.1", diast:"92", sist:"132", kisik:"90.8"},
      {cas:"2019-05-17", teza:"104.8", diast:"91", sist:"138", kisik:"94.4"},
      {cas:"2019-05-18", teza:"105.0", diast:"95", sist:"145", kisik:"92.9"}
    ]
  }

  var ana_ban = {
    ime: "Ana",
    priimek: "Ban",
    spol: "FEMALE",
    rojstvo: "2004-03-04",
    visina: "170",
    meritve: [
      {cas:"2019-05-01", teza:"54.7", diast:"77", sist:"116", kisik:"98.3"},
      {cas:"2019-05-02", teza:"54.8", diast:"75", sist:"117", kisik:"99.2"},
      {cas:"2019-05-03", teza:"54.6", diast:"78", sist:"119", kisik:"97.6"},
      {cas:"2019-05-04", teza:"55.1", diast:"76", sist:"113", kisik:"96.4"},
      {cas:"2019-05-05", teza:"55.2", diast:"77", sist:"115", kisik:"95.3"},
      {cas:"2019-05-06", teza:"54.8", diast:"74", sist:"117", kisik:"98.8"},
      {cas:"2019-05-07", teza:"54.6", diast:"78", sist:"116", kisik:"97.8"},
      {cas:"2019-05-08", teza:"54.9", diast:"79", sist:"116", kisik:"98.7"},
      {cas:"2019-05-09", teza:"54.8", diast:"76", sist:"118", kisik:"96.3"},
      {cas:"2019-05-10", teza:"55.0", diast:"76", sist:"117", kisik:"98.1"},
      {cas:"2019-05-11", teza:"55.1", diast:"78", sist:"114", kisik:"98.3"},
      {cas:"2019-05-12", teza:"54.2", diast:"79", sist:"118", kisik:"97.9"},
      {cas:"2019-05-13", teza:"54.9", diast:"75", sist:"115", kisik:"95.2"},
      {cas:"2019-05-14", teza:"54.7", diast:"82", sist:"124", kisik:"91.7"}, // obolenje, hospitalizacija
      {cas:"2019-05-15", teza:"54.3", diast:"88", sist:"132", kisik:"86.1"},
      {cas:"2019-05-16", teza:"54.0", diast:"92", sist:"128", kisik:"82.8"},
      {cas:"2019-05-17", teza:"53.9", diast:"89", sist:"138", kisik:"78.1"},
      {cas:"2019-05-18", teza:"53.7", diast:"95", sist:"142", kisik:"74.6"}
    ]
  } 

var osebePodatki = [peter_silj, dino_zaver, ana_ban];

// Ustvarjene osebe - EhrID
var osebeEhrId = ["010b6295-440e-4839-afef-35ab5afbcbae", "7b9da8d8-8102-429d-bafa-e3de9660d01e", "eabaa22d-8496-4f3b-903b-812c4ba33bea"];

// Grafi meritve
var grafTeza = {};
var grafItm = {};
var grafTlak = {};
var grafAha = {};
var grafKisik = {};

// Vremenski indikatorji
var vremenskiIndikatorji;
var grafIndikatorTek = {};
var grafIndikatorAstma = {};

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
  var oseba = osebePodatki[stPacienta - 1];

  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });

  // Ustvari novo osebo in pridobi njen ehrID
  // kot na: https://www.ehrscape.com/examples.html#_create_a_new_patient
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
      ehrId = data.ehrId;

      // build party data
      var partyData = {
        firstNames: oseba.ime,
        lastNames: oseba.priimek,
        dateOfBirth: oseba.rojstvo,
        partyAdditionalInfo: [{
          key: "ehrId",
          value: ehrId
        }]
      };
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function (party) {
          if (party.action == 'CREATE') {
            // dodajanje meritev
            // pretvorba naših podatkov v format, ki ga razume API
            var meritve = oseba.meritve;

            for (var i = 0; i < meritve.length; i++) {
              var stevecOk = 0;
              var stevecNot = 0;
              
              var meritev = meritve[i];

              var queryParams = {
                "ehrId": ehrId,
                templateId: 'Vital Signs',
                format: 'FLAT',
                committer: 'TriSrcaApp'
              };

              var compositionData = {
                "ctx/time": meritev.cas,
                "ctx/language": "en",
                "ctx/territory": "SI",
                "vital_signs/blood_pressure/any_event/systolic": meritev.sist,
                "vital_signs/blood_pressure/any_event/diastolic": meritev.diast,
                "vital_signs/height_length/any_event/body_height_length": oseba.visina,
                "vital_signs/body_weight/any_event/body_weight": meritev.teza,
                "vital_signs/indirect_oximetry/spo2|numerator": meritev.kisik
              };

              $.ajax({
                url: baseUrl + "/composition?" + $.param(queryParams),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(compositionData),
                success: function (res) {
                  stevecOk = stevecOk + 1;

                  if ((stevecOk + stevecNot > meritve.length - 1) && stevecNot < 1) {
                    var sporocilo = '<div class="alert alert-success" role="alert">Dodajanje osebe in meritev je bilo uspešno.<button type="button" class="close" data-dismiss="alert" aria-label="Zapri"><span aria-hidden="true">&times;</span></button></div>';
                    $("#vsebina").append(sporocilo);
                    nastaviEhrOsebe(ehrId, stPacienta);
                  }
                },
                error: function (res) {
                  stevecNot = StevecNot + 1;

                  if ((stevecOk + stevecNot > meritve.length - 1) && stevecNot > 0) {
                    var sporocilo = '<div class="alert alert-error" role="alert">Dodajanje osebe in meritev ni bilo uspešno.<button type="button" class="close" data-dismiss="alert" aria-label="Zapri"><span aria-hidden="true">&times;</span></button></div>';
                    $("#vsebina").append(sporocilo);
                  }
                }
              });

            }
          }
        }
      });
    }
  });

}

// izrišejo se prazni grafi za prikaz indeksa telesne mase, krvnega tlaka in nasičenosti krvi s kisikom
function narisiPrazneGrafe() {
  var naslednjiDan = dobiNaslednjiDan();
  var sestDniNazaj = dobiSestDniNazaj();

  // teza
  grafTeza.data = google.visualization.arrayToDataTable([
    ["teza", "Nič"],
    [new Date(sestDniNazaj), 0],
    [new Date(naslednjiDan), 0]
  ]);

  grafTeza.options = {
    title: "teža [kg]",
    height: 300,
    curveType: "function",
    legend: {
      position: 'bottom'
    }
  };

  grafTeza.graf = new google.visualization.LineChart(document.getElementById('grafTeza'));


  // indeks telesne mase
  grafItm.data = google.visualization.arrayToDataTable([
    ["ITM", "Normalna teža", "Povečana teža", "Debelost"],
    [new Date(sestDniNazaj), 18.5, 25, 30],
    [new Date(naslednjiDan), 18.5, 25, 30]
  ]);

  grafItm.options = {
    title: "indeks telesne mase",
    height: 300,
    curveType: "function",
    legend: {
      position: 'bottom'
    }
  };

  grafItm.graf = new google.visualization.LineChart(document.getElementById('grafItm'));

  // krvni tlak
  grafTlak.data = google.visualization.arrayToDataTable([
    ["krvni tlak", "Nič"],
    [new Date(sestDniNazaj), 0],
    [new Date(naslednjiDan), 0]
  ]);

  grafTlak.options = {
    title: "krvni tlak [mmHg]",
    height: 300,
    curveType: "function",
    legend: {
      position: 'bottom'
    }
  };

  grafTlak.graf = new google.visualization.LineChart(document.getElementById('grafTlak'));

  // AHA kategorizacija
  grafAha.data = google.visualization.arrayToDataTable([
    ["AHA kategorizacija krvnega tlak", "Normalen", "Povišan", "Hipertenzija - st1", "Hipertenzija - st2"],
    [new Date(sestDniNazaj), 1, 2, 3, 4],
    [new Date(naslednjiDan), 1, 2, 3, 4]
  ]);

  grafAha.options = {
    title: "AHA kategorizacija krvnega tlak",
    height: 300,
    curveType: "function",
    legend: {
      position: 'bottom'
    }
  };

  grafAha.graf = new google.visualization.LineChart(document.getElementById('grafAha'));

  // nasičenost krvi s kisikom
  grafKisik.data = google.visualization.arrayToDataTable([
    ["nasičenost krvi s kisikom", "Nič"],
    [new Date(sestDniNazaj), 0],
    [new Date(naslednjiDan), 0]
  ]);

  grafKisik.options = {
    title: "nasičenost krvi s kisikom [%]",
    height: 300,
    curveType: "function",
    legend: {
      position: 'bottom'
    }
  };

  grafKisik.graf = new google.visualization.LineChart(document.getElementById('grafKisik'));

  izrisGrafov();
}

// Izris 'ogrodja' grafov, le legende, brez podatkov
function izrisGrafov() {
  // povecaj vsa master/detail polja
  // Google Charts ima težavo če je graf v elementu, ki je trenutno neaktiven - ne more dobiti širine
  $("#meritveOsebe .tab-content .tab-pane").addClass("active");
  $("#meritveOsebe .nav-pills li").removeClass("active");
  
  grafTeza.graf.draw(grafTeza.data, grafTeza.options);
  grafItm.graf.draw(grafItm.data, grafItm.options);
  grafTlak.graf.draw(grafTlak.data, grafTlak.options);
  grafAha.graf.draw(grafAha.data, grafAha.options);
  grafKisik.graf.draw(grafKisik.data, grafKisik.options);

  $("#meritveOsebe .tab-content .tab-pane").removeClass("active");
  $("#meritveOsebe .tab-content .tab-pane").first().addClass("active");
  $("#meritveOsebe .nav-pills li").first().addClass("active");
}

// v vsakem trenutku, funkcija vrne trenutek čez natanko 24ur
function dobiNaslednjiDan() {
  var dan = 60 * 60 * 24 * 1000; // milisekund
  var danes = (new Date()).getTime();

  var jutri = danes + dan;
  return new Date(jutri).toISOString();
}

// v vsakem trenutku, funkcija vrne trenutek natanko pred 6 dnevi
function dobiSestDniNazaj() {
  var sestDni = 6 * 60 * 60 * 24 * 1000; // milisekund
  var danes = (new Date()).getTime();

  var sestDniNazaj = danes - sestDni;
  return new Date(sestDniNazaj).toISOString();
}

// ob pritisku gumba Generiraj podatke se ustvarijo 3 fiktivne osebe z meritvami
function ustvariOsebe() {
  oseba1 = generirajPodatke(1);
  oseba2 = generirajPodatke(2);
  oseba3 = generirajPodatke(3);
}

/**
 * Ponastavi vnešene vrednosti predpripravljenih oseb.
 * Ker se na podlagi te vrednosti napolni 'dropdown' meni, je potrebno
 * ustvariti tudi tega.
 * @param ehrId ehrID novo ustvarjene osebe
 * @param stOsebe številka osebe za vodenje internega stanja aplikacije
 * @return ehrId generiranega pacienta
 */
function nastaviEhrOsebe(ehrId, stOsebe) {
  osebeEhrId[stOsebe - 1] = ehrId;
  napolniMeni();
}

/**
 * Na podlagi internega stanja aplikacije se napolni 'dropdown' meni
 */
function napolniMeni() {
  var nazivi = [];
  var ehrId = [];

  for (var i = 0; i < osebeEhrId.length; i++) {
    nazivi.push(osebePodatki[i].ime + " " + osebePodatki[i].priimek);
    ehrId.push(osebeEhrId[i]);
  }

  // izbriši vnose v dropdownu
  $("#ustvarjeneOsebe li").remove();

  for (var i = 0; i < nazivi.length; i++) {
    var a = '<li><a href="#" val="' + ehrId[i] + '">' + nazivi[i] + '</a></li>';
    $("#ustvarjeneOsebe").append(a);
  }
}

/**
 * Definira osnovno vedenje dela strani, ki skrbi za izbiro ehrID.
 * Ta je možna z uporabo 'dropdown' menija (predpripravljene osebe)
 * oz. z vnosom ehrID v vnosno polje.
 */
function poveziGumbeMenija() {
  // vrednost iz dropdowna v vnosno polje
  $(document).on("click", "#ustvarjeneOsebe a", function () {
    var ehrId = $(this).attr('val');
    $("#ehrIdVnosnoPolje").val(ehrId);
  });

  // ob dvojnem kliku se vrednost v vnonsnem polju zbriše
  $(document).on("dblclick", "#ehrIdVnosnoPolje", function () {
    $(this).val("");
  });

  // gumb Prikaži, ki omogoča nadaljnjo interakcijo z aplikacijo
  $(document).on("click", "#prikaziPodatke", function () {
    var ehrId = $("#ehrIdVnosnoPolje").val();

    if (ehrId === null || ehrId.length < 1) {
      return;
    }

    pridobiPodatke(ehrId);
  });
}

// Iz obrazca se preberje vrednosti.
// Opravi se osnovna validacija. Kliče se funkcija, ki opravi POST zahtevo.
function poveziGumbOddajMeritev() {
  $(document).on("click", "#oddajMeritev", function () {
    var ehrId = $("#vnosEhrId").val();
    var datum = $("#vnosDatum").val();
    var teza = $("#vnosTeza").val();
    var visina = $("#vnosVisina").val();
    var diastolicniTlak = $("#vnosDiastolicni").val();
    var sistolicniTlak = $("#vnosSistolicni").val();
    var kisik = $("#vnosKisik").val();

    // osnovna validacija
    if(ehrId == "" || ehrId.length < 1) {
      napakaPriIzpolnjevanjuObrazca();
      return;
    }

    if(datum == "" || datum.length < 1) {
      napakaPriIzpolnjevanjuObrazca();
      return;
    }

    if(teza == "" || teza.length < 1) {
      napakaPriIzpolnjevanjuObrazca();
      return;
    }

    if(visina == "" || visina.length < 1) {
      napakaPriIzpolnjevanjuObrazca();
      return;
    }

    if(diastolicniTlak == "" || diastolicniTlak.length < 1) {
      napakaPriIzpolnjevanjuObrazca();
      return;
    }

    if(sistolicniTlak == "" || sistolicniTlak.length < 1) {
      napakaPriIzpolnjevanjuObrazca();
      return;
    }

    if(kisik == "" || kisik.length < 1) {
      napakaPriIzpolnjevanjuObrazca();
      return;
    }

    oddajMeritev(ehrId, datum, teza, visina, diastolicniTlak, sistolicniTlak, kisik);

  });

}

/**
 * Tu se opravi POST zahteva za dodajanje podatkov.
 * 
 * @param ehrId 
 * @param datum 
 * @param teza 
 * @param visina 
 * @param diastolicniTlak
 * @param sistolicniTlak 
 * @param kisik nasičenost, kisika v krvi
 */
function oddajMeritev(ehrId, datum, teza, visina, diastolicniTlak, sistolicniTlak, kisik) {

  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });

  // Ustvari novo osebo in pridobi njen ehrID
  // kot na: https://www.ehrscape.com/examples.html#_create_a_new_patient
  var queryParams = {
    "ehrId": ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: 'TriSrcaApp'
  };

  var compositionData = {
    "ctx/time": datum,
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/blood_pressure/any_event/systolic": sistolicniTlak,
    "vital_signs/blood_pressure/any_event/diastolic": diastolicniTlak,
    "vital_signs/height_length/any_event/body_height_length": visina,
    "vital_signs/body_weight/any_event/body_weight": teza,
    "vital_signs/indirect_oximetry/spo2|numerator": kisik
  };

  $.ajax({
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {
      var obvestilo = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Meritev ste uspešno dodali.</div>';
      $("#dodaj .panel-body").prepend(obvestilo);
    },
    error: function (res) {
      var obvestilo = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Pri prenosu je prišlo do napake. Meritve niste dodali.</div>';
      $("#dodaj .panel-body").prepend(obvestilo);
    }
  });
}

// Prikaže spročilo ob obrazcu
function napakaPriIzpolnjevanjuObrazca() {
  var obvestilo = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Obrazec vsebuje napake. Preverite vrednosti.</div>';
  $("#dodaj .panel-body").prepend(obvestilo);
}

/**
 * Pridobi osnovne podatke za določen ehrID
 * @param ehrId ehrID novo ustvarjene osebe
 */
function pridobiPodatke(ehrId) {
  var stevecOk = 0;
  var stevecNot = 0;
  var klici = 5;
  var podatki = {
    ehrId: ehrId
  };

  // 1 - osnovni podatki o osebi - ime, priimek, itd.
  $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      stevecOk = stevecOk + 1;
      podatki.osnovni = res.party;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    },
    error: function (res) {
      stevecNot = stevecNot + 1;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    }
  });

  // 2 - meritve višine
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      stevecOk = stevecOk + 1;
      podatki.visina = res;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    },
    error: function (res) {
      stevecNot = stevecNot + 1;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    }
  });

  // 3 - meritve teže
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      stevecOk = stevecOk + 1;
      podatki.teza = res;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    },
    error: function (res) {
      stevecNot = stevecNot + 1;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    }
  });

  // 4 - meritve krvnega tlaka
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      stevecOk = stevecOk + 1;
      podatki.tlak = res;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    },
    error: function (res) {
      stevecNot = stevecNot + 1;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    }
  });

  // 5 - meritve nasičenost krvi s kisikom
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/spO2",
    type: 'GET',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      stevecOk = stevecOk + 1;
      podatki.kisik = res;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    },
    error: function (res) {
      stevecNot = stevecNot + 1;

      if (stevecOk + stevecNot > klici - 1) {
        ovrednotiPodatke(podatki, stevecNot);
      }
    }
  });

}

/**
 * Ovrednosti podatke oz. izpiši obvestilo, da so podatki nepopolni
 * 
 * @param podatki podatki o osebi - ime, priimek, rojstvo, teža, višina, krvni tlak, nasičenost krvi s kisikom
 * @param stevecNot indikator, ali je ob prenosu podatkov prišlo do napake
 */
function ovrednotiPodatke(podatki, stevecNot) {
  if(stevecNot > 0) {
    var sporocilo = '<div class="alert alert-danger" role="alert">Ob pridobivanju podatkov je prišlo do napake. Poskusite ponovno pritisnit gumb <i>Prikaži</i>.<button type="button" class="close" data-dismiss="alert" aria-label="Zapri"><span aria-hidden="true">&times;</span></button></div>';;
    $("#obmocjePrikazPodatkov").append(sporocilo);

    return;
  }

  // obdelava podatkov, preoblikovanje v ustreznejši format
  var podatki1 = obdelajPodatke(podatki);

  if(podatki1 === null) {
    var sporocilo = '<div class="alert alert-danger" role="alert">Ob obdelavi pridobljenih podatkov je prišlo do napake. Poskusite ponovno pritisnit gumb <i>Prikaži</i>.<button type="button" class="close" data-dismiss="alert" aria-label="Zapri"><span aria-hidden="true">&times;</span></button></div>';;
    $("#obmocjePrikazPodatkov").append(sporocilo);

    return;
  }

  // oceni primenost osebe za udeležbo
  var primernostOsebe = oceniPrimernostOsebe(podatki1);
  
  // v obrazec za oddajanje meritev vstavi ehrId trenutnega uporabnika
  $("#vnosEhrId").val(podatki1.ehrId);
  $("#vnosVisina").val(podatki1.visina);

  // vizualiziraj podatke
  izpisOsnovnihPodatkovOsebe(podatki1);
  izrisiGrafe(podatki1);
  izrisiPrimernostOsebe(primernostOsebe);
  izrisiKoristneNapotke(primernostOsebe); // zunanji vir
}

/**
 * Podatki se preoblikujejo v ustreznejši format.
 * Glede na osnovne podatke se izračunajo dodatne vrednosti
 * - rojstvo -> starost v letih na dan izvedbe
 * - teža in višina -> indeks telesne
 * - krvni tlak -> kategorija vrednosti krvnega tlaka po AHA
 * Ostanejo le meritve za zadnjih 5 dni, glede na trenutek izvedbe.
 * 
 * @param podatki podatki o osebi - ime, priimek, rojstvo, teža, višina, krvni tlak, nasičenost krvi s kisikom
 * @return preoblikovani podatki
 */
function obdelajPodatke(podatki) {
  // osnovni podatki ime, priiimek, rojstvo, visina(neka meritev - predvideva se da so vse vrednosti iste)
  var data = {};
  data.ehrId = podatki.ehrId;
  data.ime = podatki.osnovni.firstNames;
  data.priimek = podatki.osnovni.lastNames;
  data.rojstvo = podatki.osnovni.dateOfBirth;
  data.starost = pridobiStarost(data.rojstvo);
  
  if(podatki.visina.length < 1) {
    return null;
  }

  data.visina = podatki.visina[0].height;

  // periodični podatki - teža, tlak, kisik - meritve za zadnjih 5 dni glede na današnji dan
  var danes = (new Date).getTime(); // UNIX epoch
  var petDni = 60 * 60 * 24 * 5 * 1000; // 5 dni v milisekundah
  var meja = danes - petDni;
  
  // teža in izračun indeksa telesne mase
  var podatkiTeza = [];
  for(var i = 0; i < podatki.teza.length; i++) {
    let cas = podatki.teza[i].time;
    cas = new Date(cas).getTime();
    
    if(cas > meja) {
      var teza = {
        casUnix: cas,
        casFormatiran: podatki.teza[i].time,
        teza: podatki.teza[i].weight,
        itm: izracunajITM(data.visina, podatki.teza[i].weight)
      }

      podatkiTeza.push(teza);
    }

  }

  if(podatkiTeza.length < 1) {
    return null;
  }

  data.teza = podatkiTeza;

  // nasičenost kisika
  var podatkiKisik = [];
  for(var i = 0; i < podatki.kisik.length; i++) {
    let cas = podatki.kisik[i].time;
    cas = new Date(cas).getTime();
    
    if(cas > meja) {
      var kisik = {
        casUnix: cas,
        casFormatiran: podatki.kisik[i].time,
        nasicenost: podatki.kisik[i].spO2
      }

      podatkiKisik.push(kisik);
    }

  }

  if(podatkiKisik.length < 1) {
    return null;
  }

  data.kisik = podatkiKisik;

  // krvni tlak in AHA klasifikacija za krvni tlak
  var podatkiTlak = [];
  for(var i = 0; i < podatki.tlak.length; i++) {
    let meritevKrvnegaTlaka = podatki.tlak[i];
    let cas = meritevKrvnegaTlaka.time;
    cas = new Date(cas).getTime();
    
    if(cas > meja) {
      var meritev = {
        casUnix: cas,
        casFormatiran: podatki.kisik[i].time,
        diastolicniTlak: meritevKrvnegaTlaka.diastolic,
        sistolicniTlak: meritevKrvnegaTlaka.systolic,
        ahaKategorija: dobiAhaKategorijo(meritevKrvnegaTlaka.diastolic, meritevKrvnegaTlaka.systolic)
      }

      podatkiTlak.push(meritev);
    }

  }

  if(podatkiTlak.length < 1) {
    return null;
  }

  data.tlak = podatkiTlak;

  return data;
}

/**
 * Dobi starost iz datuma rojstva, glede na trenutek uporabe
 * vir: https://stackoverflow.com/a/7091965 dne 18.05.2019
 * 
 * @param dateString datum v obliki recimo 2018-01-25
 * @return starost v letih
 */
function pridobiStarost(dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }
  return age;
}

/**
 * Izračunaj indeks telesne mase
 * formula: https://enemon.si/prehrana-in-zdravje/energija/itm-indeks-telesne-mase/ dne 18.05.2019
 * 
 * @param visina visina osebe v cm
 * @param teza teza osebe v kg
 * @return indeks telesne mase
 */
function izracunajITM(visina, teza) {
  var visinaMetri = visina / 100;

  return teza / (visinaMetri * visinaMetri)
}

/**
 * Pridobi kategorijo tlaka na podlagi AHA kategorizacije
 * formula: https://forum.facmedicine.com/data/MetaMirrorCache/22d93d82c0c01702681e74e832adcbe7.jpg dne 18.05.2019
 * 
 * @param diastolicniTlak diastolični tlak osebe
 * @param sistolicniTlak sistolični tlak osebe
 * @return kategorija krvnega tlaka
 */
function dobiAhaKategorijo(diastolicniTlak, sistolicniTlak) {
  var diast = 4;
  var sist = 4;

  // diastolični tlak
  if(diastolicniTlak < 80) {
    diast = 1
  } else if(diastolicniTlak > 80 && diastolicniTlak < 89) {
    diast = 3
  }

  // sistolicni tlak
  if(sistolicniTlak < 120) {
    sist = 1
  } else if(sistolicniTlak > 120 && sistolicniTlak < 129) {
    sist = 2
  } else if(sistolicniTlak > 129 && sistolicniTlak < 139) {
    sist = 3
  }

  console.log(diastolicniTlak, diast, sistolicniTlak, sist, Math.max(diast, sist));

  return Math.max(diast, sist);
}

/**
 * Oceni primernost osebe za udeležbo na rekreativni tekaški prireditve,
 * glede na zdravstvene podatke.
 * 
 * @param podatki podatki o osebi (že preoblikovani - glej obdelajPodatke)
 * @return ali je oseba primerna za udeležbo
 */
function oceniPrimernostOsebe(podatki) {
  /**
   * Za vsako meritev zadnjih 5 dni velja:
   * 1) ITM € [18.50, 25.00]
   * 2) Kategorija AHA < 3
   * 3) Nasičenost kisika v krvi >= 95
   */

  // ITM
  var teza = podatki.teza;
  var kriterijTezaOk = true;
  for (var i = 0; i < teza.length; i++) {
    if (teza[i].itm < 18.50 || teza[i].itm > 25.00) {
      kriterijTezaOk = false;
      break;
    }
  }

  // krvni tlak, glede na kategorizacijo AHA
  var tlak = podatki.tlak;
  var kriterijTlakOk = true;
  for (var i = 0; i < tlak.length; i++) {
    if (tlak[i].ahaKategorija > 2) {
      kriterijTlakOk = false;
      break;
    }
  }

  // nasičenost krvi s kisikom
  var kisik = podatki.kisik;
  var kriterijKisikOk = true;
  for (var i = 0; i < kisik.length; i++) {
    if (kisik[i].nasicenost < 95) {
      kriterijKisikOk = false;
      break;
    }
  }

  var skupnaPrimernost = kriterijTlakOk && kriterijTlakOk && kriterijKisikOk;

  return {
    kriterijTeza: kriterijTezaOk,
    kriterijTlak: kriterijTlakOk,
    kriterijKisik: kriterijKisikOk,
    primernost: skupnaPrimernost
  }
}

// Izris območja z osnovnimi informacijami
function izpisOsnovnihPodatkovOsebe(podatki) {
  $("#osnovniPodatkiOsebe").text("");

  $("#osnovniPodatkiOsebe").append("<h3>" + podatki.ime + " " + podatki.priimek + "</h3>");
  $("#osnovniPodatkiOsebe").append("<span>" + podatki.starost + " let, " + podatki.visina + " cm, {" + podatki.ehrId + "}</span><br><br>");
}

function izrisiGrafe(podatki) {
  // teza
  if (grafTeza.data.getNumberOfColumns() > 1) {
    grafTeza.data.removeColumn(1); // vir podatkov Nič oz. prikaz podatkov predhodne osebe
  }

  grafTeza.data.addColumn("number", "Teža");

  for (var i = 0; i < podatki.teza.length; i++) {
    var cas = podatki.teza[i].casUnix;
    cas = new Date(cas);

    var teza = podatki.teza[i].teza;
    grafTeza.data.addRow([cas, teza]);
  }

  // indeks telesne mase
  if (grafItm.data.getNumberOfColumns() > 4) {
    grafItm.data.removeColumn(4); // prikaz podatkov predhodne osebe
  }

  grafItm.data.addColumn("number", "Indeks telesne mase");

  for (var i = 0; i < podatki.teza.length; i++) {
    var cas = podatki.teza[i].casUnix;
    cas = new Date(cas);

    var itm = podatki.teza[i].itm;
    grafItm.data.addRow([cas, 18.5, 25, 30, itm]);
  }

  // krvni tlak
  if (grafTlak.data.getNumberOfColumns() > 1) {
    var zadnji = grafTlak.data.getNumberOfColumns();
    grafTlak.data.removeColumn(zadnji - 1); // vir podatkov Nič oz. prikaz podatkov predhodne osebe
  }
  if (grafTlak.data.getNumberOfColumns() > 1) {
    var zadnji = grafTlak.data.getNumberOfColumns();
    grafTlak.data.removeColumn(zadnji - 1); // vir podatkov Nič oz. prikaz podatkov predhodne osebe
  }
  // dvakrat ker dodamo dva stolpca - sisolični in diastolični tlak

  grafTlak.data.addColumn("number", "diastolični krvni tlak");
  grafTlak.data.addColumn("number", "sistolični krvni tlak");

  console.log(podatki);
  for (var i = 0; i < podatki.tlak.length; i++) {
    var cas = podatki.tlak[i].casUnix;
    cas = new Date(cas);

    var diast = podatki.tlak[i].diastolicniTlak;
    var sist = podatki.tlak[i].sistolicniTlak;
    grafTlak.data.addRow([cas, diast, sist]);
  }

  // krvni tlak - AHA kategorija
  if (grafAha.data.getNumberOfColumns() > 5) {
    grafAha.data.removeColumn(5); // prikaz podatkov predhodne osebe
  }

  grafAha.data.addColumn("number", "AHA kategorija krvnega tlaka");

  for (var i = 0; i < podatki.tlak.length; i++) {
    var cas = podatki.tlak[i].casUnix;
    cas = new Date(cas);

    var ahaKategorija = podatki.tlak[i].ahaKategorija;
    grafAha.data.addRow([cas, 1, 2, 3, 4, ahaKategorija + 0.1]);
  }

  // teza
  if (grafKisik.data.getNumberOfColumns() > 1) {
    grafKisik.data.removeColumn(1); // vir podatkov Nič oz. prikaz podatkov predhodne osebe
  }

  grafKisik.data.addColumn("number", "nasičenost krvi s kisikom");

  for (var i = 0; i < podatki.kisik.length; i++) {
    var cas = podatki.kisik[i].casUnix;
    cas = new Date(cas);

    var kisik = podatki.kisik[i].nasicenost;
    grafKisik.data.addRow([cas, kisik]);
  }

  // ponovni izris
  izrisGrafov();
}

/**
 * Izris območja z ustreznimi informacijami, primernostjo/neprimernostjo za udeležbo
 * 
 * @param primernostOsebe podatki o primenosti osebe, tj. glede na indeks telesne mase, tlak in raven kisika v krvi
 */
function izrisiPrimernostOsebe(primernostOsebe) {
  console.log(primernostOsebe);
  var pojasnilo = "Videti je, da imate sledeče težave: ";
  var arr = [];
  
  if(!primernostOsebe.kriterijKisik) {
    arr.push("nizka nasičenost kisika v krvi")
  }
  if(!primernostOsebe.kriterijTlak) {
    arr.push("previsok krvni tlak")
  }
  if(!primernostOsebe.kriterijTeza) {
    arr.push("vaša teža je previsoka")
  }

  pojasnilo = pojasnilo + arr.join(", ");

  $("#primernostNotPojasnilo").text("");

  if(primernostOsebe.primernost) {
    $("#primernostOsebe .primernostNot").hide();
    $("#primernostOsebe .primernostOk").show();
  } else {
    $("#primernostOsebe .primernostOk").hide();
    $("#primernostOsebe .primernostNot").show();

    $("#primernostNotPojasnilo").text(pojasnilo);
  }
}

/**
 * na podlagi IP naslova (https://api.ipify.org?format=json)
 * se pridobi lokacijo in za njo vremenske indikatorje za dejavnosti na prostem
 * (zadnji dve AccuWeather)
 * 
 * @param primernostOsebe podatki o primernosti osebe, tj. glede na indeks telesne mase, tlak in raven kisika v krvi
 */
function izrisiKoristneNapotke(primernostOsebe) {
  $("#napotkiZaVadbo").show();
  $("#napotki .napotkiNapaka").hide();

  // pridobi podatke o lokaciji (na podlagi IP naslova) in
  // 1-dnevno napoved za primernost teka ter o ustreznosti vadbe za bolnike z astmo - AccuWeather
  var accuWeatherUrl = "http://dataservice.accuweather.com/";
  var accuWeatherKey = "SUwafxAtBalXxtjohEImAASsZcJcCUri";

  // pridobi IP
  $.ajax({
    url: "https://api.ipify.org?format=json",
    type: 'GET',
    success: function (res) {
      var ip = res.ip;
      var url = accuWeatherUrl + "/locations/v1/cities/ipaddress?apikey=" + accuWeatherKey + "&q=" + ip;

      // AccuWeather IP -> location code
      $.ajax({
        url: url ,
        type: 'GET',
        success: function (res) {
          var locationKey = res.Key;
          var url = accuWeatherUrl + "/indices/v1/daily/1day/" + locationKey + "/groups/1?apikey=" + accuWeatherKey;

          // pridobi indikatorje za naslednji dan
          $.ajax({
            url: url ,
            type: 'GET',
            success: function (res) {
              obdelajNapoved(res);
            },
            error: function (res) {
              $("#napotki .napotkiNapaka").show();
            }
          });

        },
        error: function (res) {
          $("#napotki .napotkiNapaka").show();
        }
      });

    },
    error: function (res) {
      $("#napotki .napotkiNapaka").show();
    }
  });

}

/**
 * Napoved (podatki) se le malo preoblikujejo, ohrani se le tip indikatorja (indikator za tek, astmo) in vrednost le teh
 * 
 * @param primernostOsebe podatki o primernosti osebe, tj. glede na indeks telesne mase, tlak in raven kisika v krvi
 */
function obdelajNapoved(podatki) {
  // zanima nas vnos z ID 1 = Running
  // ter vnos z ID = 23 = Asthma
  var napovedi = [];

  for(var i = 0; i < podatki.length; i++) {
    var napoved = podatki[i];

    if(napoved.ID == 1 || napoved.ID == 23) {
      napovedi.push(napoved);
    }
  }

  // za vsako napoved nas zanima le vrednost indikatorja, ki je od 1 do 5 - vnos Value
  var skrajsanaNapoved = [];
  for(var i = 0; i < napovedi.length; i++) {
    let id = napovedi[i].ID;
    let value = napovedi[i].Value;

    skrajsanaNapoved.push({id: id, value: value});
  }

  vremenskiIndikatorji = skrajsanaNapoved;
  
  // Google Charts
  google.charts.load('current', {'packages':['gauge']});
  google.charts.setOnLoadCallback(izrisiVremenskeIndikatorje);

}
// Izrišeta se dva merilnika
// Indikatorja za tek in astmo sta v vrednosti 0-10,7
// taka je tudi min in max vrednost kazalnikov
function izrisiVremenskeIndikatorje() {
  var napoved = vremenskiIndikatorji;

  // dobi ustreznost za tek
  var tek = 0;
  var astma = 0;

  for(var i = 0; i < napoved.length; i++) {
    if(napoved[i].id == 1) {
      tek = napoved[i].value;
    } else if(napoved[i].id == 23) {
      astma = napoved[i].value;
    }
  }

  // tek
  grafIndikatorTek.data = google.visualization.arrayToDataTable([
    ['Label', 'Value'],
    ['', tek],
  ]);

  grafIndikatorTek.options = {
    width: 400,
    height: 120,
    redFrom: 8,
    redTo: 10,
    yellowFrom: 6.5,
    yellowTo: 8,
    minorTicks: 5,
    max: 10
  };

  grafIndikatorTek.graf = new google.visualization.Gauge(document.getElementById('grafIndikatorTek'));
  grafIndikatorTek.graf.draw(grafIndikatorTek.data, grafIndikatorTek.options);

  // astma
  grafIndikatorAstma.data = google.visualization.arrayToDataTable([
    ['Label', 'Value'],
    ['', astma],
  ]);

  grafIndikatorAstma.options = {
    width: 400,
    height: 120,
    redFrom: 8,
    redTo: 10,
    yellowFrom: 6.5,
    yellowTo: 8,
    minorTicks: 5,
    max: 10
  };

  grafIndikatorAstma.graf = new google.visualization.Gauge(document.getElementById('grafIndikatorAstma'));
  grafIndikatorAstma.graf.draw(grafIndikatorAstma.data, grafIndikatorAstma.options);
}

// izris aplikacije, ko je HTML dokument dokončno naložen
$(document).ready(function () {
  napolniMeni();
  poveziGumbeMenija();
  poveziGumbOddajMeritev();
});

